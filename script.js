function createNewUser() {

    let firstName = prompt('Enter your name', '');
    let lastName = prompt('Enter your second name', '');

    return {
        firstName: firstName,
        lastName: lastName,
        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + lastName.toLowerCase()
        }
    };
}

let user01 = createNewUser();
user01.getLogin();

console.log(user01.getLogin());
